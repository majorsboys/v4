#pragma once
#include "Configuration.hpp"
#include "dropboxes.h"
#include "Variables.h"
#include "Themes.h"
#include <chrono>
//ImFont* fTabs;
//
#define IM_ARRAYSIZE(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))

typedef void(*CL_FullUpdate_t) (void);
CL_FullUpdate_t CL_FullUpdate = nullptr;
void KnifeApplyCallbk()
{
	static auto CL_FullUpdate = reinterpret_cast<CL_FullUpdate_t>(U::FindPattern("engine.dll", reinterpret_cast<PBYTE>("\xA1\x00\x00\x00\x00\xB9\x00\x00\x00\x00\x56\xFF\x50\x14\x8B\x34\x85"), "x????x????xxxxxxx"));
	CL_FullUpdate();
}
void dankestSetClanTag(const char* tag, const char* name)
{
	static auto ClanTagOffset = U::FindPattern("engine.dll", (PBYTE)"\x53\x56\x57\x8B\xDA\x8B\xF9\xFF\x15", "xxxxxxxxx");
	if (ClanTagOffset)
	{
		if (strlen(name) > 0) {
			auto name_ = std::string(name);
			auto newline = name_.find("\\n");
			auto tab = name_.find("\\t");
			if (newline != std::string::npos) {
				name_.replace(newline, newline + 2, "\n");
			}
			if (tab != std::string::npos) {
				name_.replace(tab, tab + 2, "\t");
			}
		}
		auto tag_ = std::string(tag);
		if (strlen(tag) > 0) {
			auto newline = tag_.find("\\n");
			auto tab = tag_.find("\\t");
			if (newline != std::string::npos) {
				tag_.replace(newline, newline + 2, "\n");
			}
			if (tab != std::string::npos) {
				tag_.replace(tab, tab + 2, "\t");
			}
		}
		static auto dankesttSetClanTag = reinterpret_cast<void(__fastcall*)(const char*, const char*)>(ClanTagOffset);
		dankesttSetClanTag(tag_.data(), tag_.data());
	}
}

void RenderSkinWindow()
{

}

/*void RenderEventLog(IGameEvent* event)
{
	auto& style = ImGui::GetStyle();
	style.Alpha = 1.f;
	//ImGui::PushFont(Global::fDefault);
	style.WindowMinSize = ImVec2(246, 40);
	style.WindowPadding = ImVec2(8, 8);
	style.ItemSpacing = ImVec2(4, 4);

	if (ImGui::Begin("Event Log", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
	{


		C_BaseEntity* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
		if (!strcmp(event->GetName(), "item_purchase"))
		{
			auto buyer = event->GetInt("userid");
			std::string gun = event->GetString("weapon");


			if (strstr(gun.c_str(), "molotov")
				|| strstr(gun.c_str(), "nade")
				|| strstr(gun.c_str(), "kevlar")
				|| strstr(gun.c_str(), "decoy")
				|| strstr(gun.c_str(), "suit")
				|| strstr(gun.c_str(), "flash")
				|| strstr(gun.c_str(), "vest")
				|| strstr(gun.c_str(), "cutter")
				|| strstr(gun.c_str(), "defuse")
				)  return;


			auto player_index = g_Engine->GetPlayerForUserID(buyer);
			C_BaseEntity* player = (C_BaseEntity*)g_EntityList->GetClientEntity(player_index);
			player_info_t pinfo;

			if (player && local &&  player->GetTeamNum() != local->GetTeamNum() && g_Engine->GetPlayerInfo(player_index, &pinfo))
			{

				if (g_ChatElement)
				{
					gun.erase(gun.find("weapon_"), 7);
					// g_ChatElement->ChatPrintf(0, 0, " ""\x04""%s bought %s\n", pinfo.name, gun.c_str());
					ImGui::Text("[STAY.HIGH] ""\x04""%s bought %s\n", pinfo.name, gun.c_str());
				}
			}
		}
	}ImGui::End();
}*/
void color()
{
	//style.Colors[ImGuiCol_Button] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	ImGuiStyle& style = ImGui::GetStyle();
	style.Colors[ImGuiCol_Text] = ImVec4(0.86f, 0.93f, 0.89f, 0.78f);
	style.Colors[ImGuiCol_TextDisabled] = ImVec4(0.86f, 0.93f, 0.89f, 0.78f);
	style.Colors[ImGuiCol_WindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	style.Colors[ImGuiCol_CheckMark] = ImColor(1, 1, 1, 255);

	style.Colors[ImGuiCol_ChildWindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	style.Colors[ImGuiCol_Border] = ImColor(15, 30, 15, 255);
	style.Colors[ImGuiCol_FrameBg] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_FrameBgHovered] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_FrameBgActive] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_TitleBg] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_TitleBgActive] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_MenuBarBg] = ImVec4(0.35f, 0.35f, 0.35f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarBg] = ImVec4(0.13f, 0.13f, 0.13f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.09f, 0.15f, 0.16f, 1.00f);
	style.Colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.15f, 0.60f, 0.78f, 0.78f);
	style.Colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.15f, 0.60f, 0.78f, 1.00f);

	style.Colors[ImGuiCol_SliderGrabActive] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_Button] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	style.Colors[ImGuiCol_ButtonHovered] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	style.Colors[ImGuiCol_ButtonActive] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	style.Colors[ImGuiCol_Header] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_HeaderHovered] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_HeaderActive] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);
	style.Colors[ImGuiCol_ColumnHovered] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_CloseButton] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
	style.Colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.184f, 0.310f, 0.310f, 0.95f);
}

int i;
float shibe;
char bombdamagestring1[24];
char bombdamagestring2[24];
void RenderC4Info() { // not using cuz it's crashing and i can't code it properly lmfao


	auto& style = ImGui::GetStyle();
	style.WindowMinSize = ImVec2(246, 40);
	style.WindowPadding = ImVec2(8, 8);
	style.ItemSpacing = ImVec2(4, 4);
	if (ImGui::Begin("C4 Info", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize))
	{
		C_BaseEntity* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
		C_BaseEntity* entity = g_EntityList->GetClientEntity(i);
		Vector vOrig; Vector vScreen;
		vOrig = entity->GetOrigin();
		CCSBomb* Bomb = (CCSBomb*)entity;
		float flBlow = Bomb->GetC4BlowTime();
		float lifetime = flBlow - (g_Globals->interval_per_tick * local->GetTickBase());
		float flDistance = local->GetEyePosition().DistTo(entity->GetEyePosition());
		float a = 450.7f;
		float b = 75.68f;
		float c = 789.2f;
		float d = ((flDistance - b) / c);
		float flDamage = a*exp(-d * d);
		shibe = float((std::max)((int)ceilf(CSGO_Armor(flDamage, local->ArmorValue())), 0));
		sprintf_s(bombdamagestring1, sizeof(bombdamagestring1) - 1, "FATAL");
		sprintf_s(bombdamagestring2, sizeof(bombdamagestring2) - 1, "%.0fdmg", shibe);
		if (local->IsAlive())
		{
			if (lifetime > -2.f)
			{
				if (shibe >= local->GetHealth())
				{
					//g_Render->DrawString2(g_Render->font.Indicators, 5, 75, Color(255, 40, 40, 255), FONT_LEFT, bombdamagestringdead);
					ImGui::TextColored(ImVec4(1.f, 0.40f, 0.40f, 1.f), "%s", bombdamagestring1);
				}
				else if (local->GetHealth() > shibe)
				{
					//g_Render->DrawString2(g_Render->font.Indicators, 5, 75, Color(250, 180, 50, 255), FONT_LEFT, bombdamagestringalive);
					ImGui::TextColored(ImVec4(0.25f, 0.18f, 0.50f, 1.f), "%s", bombdamagestring2);

				}
			}
		}

		char buffer[64];
		if (lifetime > 0.01f && !Bomb->IsBombDefused())
		{
			sprintf_s(buffer, "%.1fs", lifetime);
			float countdown = Bomb->GetC4DefuseCountDown() - (local->GetTickBase() * g_Globals->interval_per_tick);
			//g_Render->DrawString2(g_Render->font.Indicators, 5, 50, Color(255, 200, 0, 255), FONT_LEFT, buffer);
			ImGui::TextColored(ImVec4(1.f, 0.2f, 0.f, 1.f), "%s", buffer);
		}

		int width;
		int height;
		g_Engine->GetScreenSize(width, height);
		int halfX = width / 2;
		int halfY = height / 2;

		if (Bomb->GetBombDefuser() > 0)
		{
			float countdown = Bomb->GetC4DefuseCountDown() - (local->GetTickBase() * g_Globals->interval_per_tick);
			if (countdown > 0.01f)
			{
				if (lifetime > countdown)
				{
					char defuseTimeString[24];
					sprintf_s(defuseTimeString, sizeof(defuseTimeString) - 1, "DEFUSE %.1f", countdown);
					//g_Render->DrawString2(g_Render->font.Defuse, 5, 100, Color(180, 80, 255, 255), FONT_LEFT, defuseTimeString);
					ImGui::TextColored(ImVec4(0.18f, 0.80f, 1.f, 1.f), "%s", defuseTimeString);
				}
			}
		}
	} ImGui::End();
}

void RenderSpecList() {
	auto& style = ImGui::GetStyle();
	//ImGui::PushFont(Global::fDefault);
	style.WindowMinSize = ImVec2(246, 40);
	style.WindowPadding = ImVec2(8, 8);
	style.ItemSpacing = ImVec2(4, 4);
	if (ImGui::Begin("Spectator List", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
	{
		C_BaseEntity* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

		RECT scrn = g_Render->GetViewport();
		int kapi = 0;
		if (local)
		{
			for (int i = 0; i < g_EntityList->GetHighestEntityIndex(); i++)
			{
				// Get the entity
				C_BaseEntity *pEntity = g_EntityList->GetClientEntity(i);
				player_info_t pinfo;
				if (pEntity && pEntity != local)
				{
					if (g_Engine->GetPlayerInfo(i, &pinfo) && !pEntity->IsAlive() && !pEntity->IsDormant())
					{
						HANDLE obs = pEntity->GetObserverTargetHandle();
						if (obs)
						{
							C_BaseEntity *pTarget = g_EntityList->GetClientEntityFromHandle(obs);
							player_info_t pinfo2;
							//if (pTarget && pTarget->GetIndex() == local->GetIndex())
							if (pTarget && pTarget->GetIndex())
							{
								if (g_Engine->GetPlayerInfo(pTarget->GetIndex(), &pinfo2))
								{
									if (pTarget->GetIndex() != local->GetIndex())
										ImGui::Text("%s --> %s", pinfo.name, pinfo2.name);
									else if (pTarget->GetIndex() == local->GetIndex())
										ImGui::TextColored(ImVec4(1, 0, 0, 1),"%s --> %s", pinfo.name, pinfo2.name);
									ImGui::Separator();
									kapi++;
								}
							}
						}
					}
				}
			}
		}
	}ImGui::End();
}

void RenderPreview() {
	ImDrawList* draw = ImGui::GetWindowDrawList();
	ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(1, 1, 1, 1));
	auto& style = ImGui::GetStyle();
	style.WindowMinSize = ImVec2(210, 40);
	style.WindowPadding = ImVec2(8, 8);
	style.ItemSpacing = ImVec2(4, 4);
	if (ImGui::Begin("ESP Preview", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoCollapse)) {
		int alpha = 0;
		const char* name = "";
		ImGui::Dummy(ImVec2(32, 0));
		ImGui::SameLine();
		if (g_Options.Visuals.Name) {
			name = "name";
		}
		ImGui::Text(name);
		ImVec2 p = ImGui::GetCursorScreenPos();
		ImGui::Spacing();
		if (g_Options.Visuals.HP) {
			alpha = 255;
		}
		else
		{
			alpha = 0;
		}
		draw->AddRect(ImVec2(p.x, p.y), ImVec2(p.x + 2, p.y + 175), ImColor(0, 255, 0, alpha), 0.0f, 15, 1.f);
		ImGui::SameLine(12);
		if (g_Options.Visuals.Box) {
			ImVec2 p1 = ImGui::GetCursorScreenPos();
			draw->AddRect(ImVec2(p1.x, p1.y), ImVec2(p1.x + 100, p1.y + 175), ImColor(255, 0, 0, 255), 0.0f, 15, 1.f);
		}

		if (g_Options.Visuals.Weapon) {
			ImGui::SameLine(116);
			ImGui::Text("weapon");
		}
	} ImGui::End();
}

void RenderInfoBox() {
	bool is_renderer_active = renderer->IsActive();

	if (g_Options.Misc.InfoBox) {
		if (!is_renderer_active) {
			CInput::CUserCmd* CurrentCmd;
			C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
			auto& style = ImGui::GetStyle();
			//ImGui::PushFont(Global::fDefault);
			int lbyred = 0;
			int lbygreen = 0;
			style.WindowMinSize = ImVec2(320, 90);
			//style.Colors[ImGuiCol_WindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
			if (ImGui::Begin("Infobox", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
			{


				if (g_Engine->IsInGame() && g_Engine->IsConnected() && pLocal->IsAlive())
				{
					if (!(CurrentCmd->viewangles.y - pLocal->GetLowerBodyYaw() >= -35 && CurrentCmd->viewangles.y - pLocal->GetLowerBodyYaw() <= 35))
					{
						lbygreen = 1;
						lbyred = 0;
					}
					else
					{
						lbyred = 1;
						lbygreen = 0;
					}
					ImGui::Text("Delta YAW: ");
					ImGui::SameLine();
					ImGui::TextColored(ImVec4(lbyred, lbygreen, 0, 1.f), "[STATUS]");

					int velocity = pLocal->GetVelocity().Length2D();
					ImGui::TextColored(ImVec4(0.97, 0.97, 0, 1), "Velocity: %i", velocity);



					int anglereal = Global::curReal;
					int anglefake = Global::curFake;
					int lby = pLocal->GetLowerBodyYaw();
					//int pitch = pLocal->GetEyeAngles()->z;
					bool sendpacket = G::SendPacket;

				//	ImGui::Text("Angle: "); ImGui::SameLine();
					//ImGui::TextColored(ImVec4(0, 0.97, 0, 1), "%i", pitch);

					ImGui::Text("bSendPacket: ");
					ImGui::SameLine();
					if (sendpacket)
						ImGui::TextColored(ImVec4(0, 1, 0, 1.f), "true");
					else
						ImGui::TextColored(ImVec4(1, 0, 0, 1.f), "false");

				}
			}ImGui::End();
		}
		else if (is_renderer_active) {
			C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
			auto& style = ImGui::GetStyle();
			//ImGui::PushFont(Global::fDefault);
			int lbyred = 0;
			int lbygreen = 0;
			style.WindowMinSize = ImVec2(320, 90);
			//style.Colors[ImGuiCol_WindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
			if (ImGui::Begin("Infobox", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
			{


				if (g_Engine->IsInGame() && g_Engine->IsConnected() && pLocal->IsAlive())
				{

					ImGui::Text("Delta YAW: *status*");
					ImGui::TextColored(ImVec4(0.97, 0.97, 0, 1), "Velocity: *length2d*");

					//	ImGui::Text("Angle: "); ImGui::SameLine();
					//ImGui::TextColored(ImVec4(0, 0.97, 0, 1), "%i", pitch);

					ImGui::Text("bSendPacket: *val*");

				}
			}ImGui::End();
		}
	}
}
extern HMODULE g_hLib;
extern HWND g_hWnd;

namespace ImGui
{
	class Tab
	{
	private:

		std::vector<std::string> labels;

	public:

		void add(std::string name)
		{
			labels.push_back(name);
		}

		void draw(int *selected)
		{
			ImGuiStyle &style = GetStyle();
			ImVec4 color = style.Colors[ImGuiCol_Button];
			ImVec4 colorActive = style.Colors[ImGuiCol_ButtonActive];
			ImVec4 colorHover = style.Colors[ImGuiCol_ButtonHovered];
			ImVec2 max = GetContentRegionMax();
			float size_x = max.x / labels.size() - 0.f;
			float size_y = max.y / labels.size() - 30.f;

			for (size_t i = 0; i < labels.size(); i++)
			{
				if (i == *selected)
				{
					style.Colors[ImGuiCol_Button] = colorActive;
					style.Colors[ImGuiCol_ButtonActive] = colorActive;
					style.Colors[ImGuiCol_ButtonHovered] = colorActive;
				}
				else
				{
					style.Colors[ImGuiCol_Button] = color;
					style.Colors[ImGuiCol_ButtonActive] = colorActive;
					style.Colors[ImGuiCol_ButtonHovered] = colorHover;
				}

				if (Button(labels.at(i).c_str(), { size_x, size_y }))
					*selected = i;
			}

			style.Colors[ImGuiCol_Button] = color;
			style.Colors[ImGuiCol_ButtonActive] = colorActive;
			style.Colors[ImGuiCol_ButtonHovered] = colorHover;
		}
	};
}

UCHAR
filesys[255],
volnamebuff[255];

DWORD
dwmfl,
dwflagssys,
dwserialactual;


#define coder (70050651) //Main Coder
#define second (1213810804) //Member-SecondCoder
#define pies (-1935764516) // Member


char* const usernames[] =
{
	"patriot",
	"miki",
	"vilnar",
	"smove"
};

char* const passwords[] =
{
	"pcoder",
	"k7c",
	"rafatus",
	"pana"
};

void RenderColorWindow() {
	Global::CurrentTab = 5;
}


void RenderConsole() {
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	auto& style = ImGui::GetStyle();
	style.Alpha = 0.9f;
	//ImGui::PushFont(Global::fDefault);
	style.WindowMinSize = ImVec2(215, 40);
	//style.Colors[ImGuiCol_WindowBg] = ImVec4(0.1f, 0.1f, 0.1f, 0.95f);
	if (ImGui::Begin("Console (test)", NULL, ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
	{
		char CommandSend[100];
		char CommandActual[100];
		CommandActual == &g_Options.Menu.password;
		ImGui::InputText("##comandhere", &g_Options.Menu.password, 100);
		ImGui::SameLine();
		if (ImGui::Button("Send"))
		{
			if (CommandActual == "dsbaa") {
				exit(0);
			}
		}

		ImGui::Text("> %s", CommandSend);

	}ImGui::End();
}


void RenderInterface() {

	bool is_renderer_active = renderer->IsActive();
	ImGuiIO& io = ImGui::GetIO();

	//RenderRay(pDevice);
	auto& style = ImGui::GetStyle();

	GetVolumeInformation("C:\\", (LPTSTR)volnamebuff, 255, &dwserialactual, &dwmfl, &dwflagssys, (LPTSTR)filesys, 255);


	if (is_renderer_active) {

		if (Global::RenderLogin) {
			Global::RenderLogin = false;
			Global::RenderMenu = true;
		} 

		if (Global::RenderMenu) {
			ImGui::SetNextWindowSize(ImVec2(1090, 621));
			style.WindowPadding = ImVec2(8, 8);
			style.ItemSpacing = ImVec2(8, 5);
			if (ImGui::Begin(XorStr("nostalgia"), NULL, ImVec2(1090, 621), 1.0f, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse))
			{
				ImGui::PushFont(fkek);
				ImGui::BeginChild("##tabarea", ImVec2(80, 110 * 5 + 27), false);
				{
					if (ImGui::Button("A", ImVec2(80, 110)))
						Global::CurrentTab = 0;
					if (ImGui::Button("D", ImVec2(80, 110)))
						Global::CurrentTab = 2;
					if (ImGui::Button("E", ImVec2(80, 110)))
						Global::CurrentTab = 3;
					if (ImGui::Button("C", ImVec2(80, 110)))
						Global::CurrentTab = 5;
					if (ImGui::Button("B", ImVec2(80, 110)))
						Global::CurrentTab = 4;
					ImGui::PopFont();
				} ImGui::EndChild();
				ImGui::SameLine();

				if (g_Options.Menu.Skinchanger1)
				{
					ImGui::SetNextWindowSize(ImVec2(400, 543));
					if (ImGui::Begin(XorStr("nostalgia | skinchanger"), NULL, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoResize))
					{
						ImGui::Checkbox(("Enabled"), &g_Options.Skinchanger.Enabled);
						ImGui::SameLine();
						ImGui::PushItemWidth(150);
						if (ImGui::Button(("Force Update")))
						{
							KnifeApplyCallbk();
						}


						//ImGui::Separator();

						ImGui::Text("General");
						ImGui::Combo(("Knife Model"), &g_Options.Skinchanger.Knife, knives, ARRAYSIZE(knives));
						ImGui::Combo(("Knife Skin"), &g_Options.Skinchanger.KnifeSkin, knifeskins, ARRAYSIZE(knifeskins));
						ImGui::Combo(("Gloves"), &g_Options.Skinchanger.gloves, gloves, ARRAYSIZE(gloves));


						ImGui::Columns(1);
						ImGui::Separator();
						ImGui::Text("Rifles");
						ImGui::Columns(2, NULL, false);
						ImGui::Combo(("AK-47"), &g_Options.Skinchanger.AK47Skin, ak47, ARRAYSIZE(ak47));
						ImGui::Combo(("M4A1-S"), &g_Options.Skinchanger.M4A1SSkin, m4a1s, ARRAYSIZE(m4a1s));
						ImGui::Combo(("M4A4"), &g_Options.Skinchanger.M4A4Skin, m4a4, ARRAYSIZE(m4a4));
						ImGui::NextColumn();
						ImGui::Combo(("AUG"), &g_Options.Skinchanger.AUGSkin, aug, ARRAYSIZE(aug));
						ImGui::Combo(("FAMAS"), &g_Options.Skinchanger.FAMASSkin, famas, ARRAYSIZE(famas));

						ImGui::Columns(1);
						ImGui::Separator();
						ImGui::Text("Snipers");
						ImGui::Columns(2, NULL, false);
						ImGui::Combo(("AWP"), &g_Options.Skinchanger.AWPSkin, awp, ARRAYSIZE(awp));
						ImGui::Combo(("SSG08"), &g_Options.Skinchanger.SSG08Skin, ssg08, ARRAYSIZE(ssg08));
						ImGui::NextColumn();
						ImGui::Combo(("SCAR20"), &g_Options.Skinchanger.SCAR20Skin, scar20, ARRAYSIZE(scar20));

						ImGui::Columns(1);
						ImGui::Separator();
						ImGui::Text("SMG's");
						ImGui::Columns(2, NULL, false);
						ImGui::Combo(("P90"), &g_Options.Skinchanger.P90Skin, p90, ARRAYSIZE(p90));
						ImGui::NextColumn();
						ImGui::Combo(("UMP45"), &g_Options.Skinchanger.UMP45Skin, ump45, ARRAYSIZE(ump45));

						ImGui::Columns(1);
						ImGui::Separator();
						ImGui::Text("Pistols");
						ImGui::Columns(2, NULL, false);
						ImGui::Combo(("Glock-18"), &g_Options.Skinchanger.GlockSkin, glock, ARRAYSIZE(glock));
						ImGui::Combo(("USP-S"), &g_Options.Skinchanger.USPSkin, usp, ARRAYSIZE(usp));
						ImGui::Combo(("Deagle"), &g_Options.Skinchanger.DeagleSkin, deagle, ARRAYSIZE(deagle));

						ImGui::NextColumn();
						ImGui::Combo(("TEC-9"), &g_Options.Skinchanger.tec9Skin, tec9, ARRAYSIZE(tec9));
						ImGui::Combo(("P2000"), &g_Options.Skinchanger.P2000Skin, p2000, ARRAYSIZE(p2000));
						ImGui::Combo(("P250"), &g_Options.Skinchanger.P250Skin, p250, ARRAYSIZE(p250));

					}ImGui::End();
				}

				ImGui::BeginChild("##tabrender", ImVec2(986, 570), true);
				{
					if (Global::CurrentTab == 0) {
						if (ImGui::Button("Aimbot", ImVec2(505 - 15, 55))) Global::RageSub = 0;
						ImGui::SameLine();
						if (ImGui::Button("Anti-Aims", ImVec2(505 - 15, 55))) Global::RageSub = 1;

						if (Global::RageSub == 0) {
							ImGui::BeginChild("##generalchild", ImVec2(480, 350), true);
							{
								ImGui::Text("						 General");
								ImGui::Checkbox("Main Switch", &g_Options.Ragebot.MainSwitch);
								ImGui::Hotkey("Aim Key", &g_Options.Ragebot.KeyPress);
								ImGui::SliderFloat("##fieldofviewaimbot", &g_Options.Ragebot.FOV, 0.f, 360.f, "Aim FOV: %.f");
								ImGui::SliderFloat("##velocitylimit", &g_Options.Ragebot.VelLimit, 0.f, 10000.f, "Max. Velocity: %.f");
								ImGui::Combo("Silent", &g_Options.Ragebot.AimbotType, aim_types, ARRAYSIZE(aim_types));
								ImGui::Checkbox("Auto-Shoot", &g_Options.Ragebot.AutoFire);
								ImGui::Combo("Resolver", &g_Options.Ragebot.pResolvery, resolvery, ARRAYSIZE(resolvery));

							} ImGui::EndChild();
							ImGui::SameLine();
							ImGui::BeginChild("##accuracychild", ImVec2(450, 350), true);
							{
								ImGui::Text("							Weapon");
								ImGui::Checkbox("Auto-Crouch", &g_Options.Ragebot.AutoCrouch);
								ImGui::Checkbox("Auto-Stop", &g_Options.Ragebot.AutoStop);
								ImGui::Checkbox("Auto-Zoom", &g_Options.Ragebot.AutoScope);
								ImGui::SliderFloat("##hitchancerage", &g_Options.Ragebot.HitchanceAmount, 0.f, 100.f, "Hitchance: %.f");
								ImGui::Checkbox("Remove Recoil", &g_Options.Ragebot.AntiRecoil);
								ImGui::Checkbox("Auto-Pistol", &g_Options.Ragebot.AutoPistol);
							} ImGui::EndChild();
							ImGui::BeginChild("##targetchild", ImVec2(265, 123), true);
							{
								ImGui::Text("			  Target");
								ImGui::SliderFloat("##baimifhpisunderxx", &g_Options.Ragebot.BaimIFHPX, 0.f, 100.f, "Baim if HP < %.f");
								ImGui::Checkbox("Friendly Fire", &g_Options.Ragebot.FriendlyFire);
								ImGui::SliderFloat("##mindamage", &g_Options.Ragebot.MinimumDamage, 0.f, 100.f, "Min. Damage: %.f");
								ImGui::SliderFloat("##bruteforcefafter", &g_Options.Ragebot.BruteforceX, 0.f, 100.f, "Brute After %.f Shots");
								ImGui::Combo("Hitbox", &g_Options.Ragebot.Hitbox, aimBones, ARRAYSIZE(aimBones));
								ImGui::Combo("Hit-Scan", &g_Options.Ragebot.Hitscan, hitscan, ARRAYSIZE(hitscan));

							} ImGui::EndChild();
						}
						else if (Global::RageSub == 1) {
							ImGui::Checkbox("Enabled", &g_Options.Ragebot.EnabledAntiAim);
						//	ImGui::SameLine();
							//ImGui::Checkbox("Delta Breaker", &g_Options.Ragebot.AntiResolvery);
							ImGui::BeginChild("##standchild", ImVec2(250, 350), true);
							{
								ImGui::Text("			Stand");
								ImGui::Combo("Pitch", &g_Options.Ragebot.Pitch, antiaimpitch, ARRAYSIZE(antiaimpitch));
								ImGui::Combo("Real Y", &g_Options.Ragebot.YawTrue, antiaimyawtrue, ARRAYSIZE(antiaimyawtrue));
								ImGui::Combo("Fake Y", &g_Options.Ragebot.YawFake, antiaimyawfake, ARRAYSIZE(antiaimyawfake));

							} ImGui::EndChild();
							ImGui::SameLine();
							ImGui::BeginChild("##movechild", ImVec2(250, 350), true);
							{
								ImGui::Text("			Move");
								ImGui::Combo("Pitch", &g_Options.Ragebot.Pitch, antiaimpitch, ARRAYSIZE(antiaimpitch));
								ImGui::Combo("Real Y", &g_Options.Ragebot.RealMove, antiaimyawtrue, ARRAYSIZE(antiaimyawtrue));
								ImGui::Combo("Fake Y", &g_Options.Ragebot.FakeMove, antiaimyawfake, ARRAYSIZE(antiaimyawfake));

							} ImGui::EndChild();
							ImGui::SameLine();
							ImGui::BeginChild("##edgechild", ImVec2(250, 350), true);
							{
								ImGui::Text("			  Edge");
								ImGui::Combo("Pitch", &g_Options.Ragebot.Pitch, antiaimpitch, ARRAYSIZE(antiaimpitch));
								ImGui::Combo("Real Y", &g_Options.Ragebot.EdgeYaw, antiaimyawfake, ARRAYSIZE(antiaimyawfake));
								ImGui::Combo("Fake Y", &g_Options.Ragebot.EdgeFake, antiaimyawfake, ARRAYSIZE(antiaimyawfake));

							} ImGui::EndChild();
							ImGui::SameLine();
							ImGui::BeginChild("##others", ImVec2(175, 350), true);
							{
								ImGui::Text("		  Other");
								ImGui::Checkbox("LBY Breaker", &g_Options.Misc.LBYBreak);
								//ImGui::SliderFloat("##lbydeltaragebot", &g_Options.Ragebot.LBYDelta, -180.f, 180.f, "Delta-Y: %.f");
								ImGui::Checkbox("Fakewalk", &g_Options.Ragebot.FakeWalk);
								ImGui::Hotkey("Flip", &g_Options.Ragebot.SwitchKey);
								ImGui::Checkbox("On Knife", &g_Options.Ragebot.KnifeAA);
							} ImGui::EndChild();
						}
					}
					else if (Global::CurrentTab == 2) {
						ImGui::BeginChild("##legitbotchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			General");
							ImGui::Checkbox("Main Switch", &g_Options.Legitbot.MainSwitch);
							ImGui::Checkbox("Enable Aimbot", &g_Options.Legitbot.Aimbot.Enabled);
							ImGui::Checkbox("Backtrack", &g_Options.Legitbot.backtrack);
							ImGui::NewLine();
							ImGui::SameLine(15);
							ImGui::SliderFloat("##ticksbacktrack", &g_Options.Legitbot.BacktrackTicks, 0.f, 25.f, "Ticks: %.f");
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##riflechild", ImVec2(250, 350), true);
						{
							ImGui::Text("			Rifles");
							ImGui::Hotkey("On Key##0", &g_Options.Legitbot.MainKey);
							ImGui::SliderFloat("##aimfovrifle", &g_Options.Legitbot.Mainfov, 0.00f, 90.00f, "Aim FOV: %.f");
							ImGui::SliderFloat("##smoothrifle", &g_Options.Legitbot.MainSmooth, 1.00f, 100.00f, "Smooth: %.f");
							ImGui::SliderFloat("##minrcsrifle", &g_Options.Legitbot.main_recoil_min, 1.00f, 100.00f, "Min. RCS: %.f");
							ImGui::SliderFloat("##maxrcsrifle", &g_Options.Legitbot.main_recoil_max, 1.00f, 100.00f, "Max. RCS: %.f");
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##pistolchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			Pistols");
							ImGui::Hotkey("On Key##1", &g_Options.Legitbot.PistolKey);
							ImGui::SliderFloat("##aimfovpistol", &g_Options.Legitbot.Pistolfov, 0.00f, 90.00f, "Aim FOV: %.f");
							ImGui::SliderFloat("##smoothpistol", &g_Options.Legitbot.PistolSmooth, 1.00f, 100.00f, "Smooth: %.f");
							ImGui::SliderFloat("##minrcspistol", &g_Options.Legitbot.pistol_recoil_min, 1.00f, 100.00f, "Min. RCS: %.f");
							ImGui::SliderFloat("##maxrcspistol", &g_Options.Legitbot.pistol_recoil_max, 1.00f, 100.00f, "Max. RCS: %.f");
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##sniperchild", ImVec2(175, 350), true);
						{
							ImGui::Text("		  Snipers");
							ImGui::Hotkey("On Key##2", &g_Options.Legitbot.SniperKey, ImVec2(60, 25));
							ImGui::SliderFloat("##aimfovsniper", &g_Options.Legitbot.Sniperfov, 0.00f, 90.00f, "Aim FOV: %.f");
							ImGui::SliderFloat("##smoothsniper", &g_Options.Legitbot.SniperSmooth, 1.00f, 100.00f, "Smooth: %.f");
							ImGui::SliderFloat("##minrcssniper", &g_Options.Legitbot.sniper_recoil_min, 1.00f, 100.00f, "Min. RCS: %.f");
							ImGui::SliderFloat("##maxrcssniper", &g_Options.Legitbot.sniper_recoil_max, 1.00f, 100.00f, "Max. RCS: %.f");
						} ImGui::EndChild();

						ImGui::BeginChild("##otherchildlegit", ImVec2(265, 123), true);
						{
							ImGui::Text("			  Extra");
							ImGui::Checkbox("Legit AA", &g_Options.Legitbot.LegitAA);
						} ImGui::EndChild();
					}
					else if (Global::CurrentTab == 3) {
						ImGui::Checkbox("Enable", &g_Options.Visuals.Enabled);
						ImGui::BeginChild("##espchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Main");
							if (ImGui::Button("ESP", ImVec2(120, 25))) Global::VisualsSub = 0;
							ImGui::SameLine();
							if (ImGui::Button("Filters", ImVec2(120, 25))) Global::VisualsSub = 1;

							if (Global::VisualsSub == 0) {
								ImGui::Selectable("Box", &g_Options.Visuals.Box);
								ImGui::Selectable("Name", &g_Options.Visuals.Name);
								ImGui::Selectable("Health", &g_Options.Visuals.HP);
								ImGui::Selectable("Weapon", &g_Options.Visuals.Weapon);
								ImGui::Selectable("Info", &g_Options.Visuals.Info);
							}
							else if (Global::VisualsSub == 1) {
								ImGui::Selectable("Players", &g_Options.Visuals.Filter.Players);
								ImGui::Selectable("Enemy Only", &g_Options.Visuals.Filter.Enemy);
								ImGui::Selectable("Visible Only", &g_Options.Visuals.Filter.VisibleOnly);
								ImGui::Selectable("Weapons", &g_Options.Visuals.WeaponsWorld);
								ImGui::Selectable("Bomb", &g_Options.Visuals.C4World);
							}
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##otherchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			Removables");
							ImGui::Checkbox("Visual Punch/Recoil", &g_Options.Visuals.NoVisualRecoil);
							ImGui::Checkbox("Smoke", &g_Options.Visuals.NoSmoke);
							ImGui::Checkbox("Flashbang", &g_Options.Visuals.NoFlash);
							ImGui::Checkbox("Scope Border", &g_Options.Visuals.noscopeborder);
							ImGui::Text("			  Extra");
							ImGui::Checkbox("Ultra Recoil", &g_Options.Visuals.ShakePunch);
							ImGui::Checkbox("Aiming-Like", &g_Options.Visuals.InverseView);
							ImGui::Checkbox("Sniper Crosshair", &g_Options.Visuals.SniperCrosshair);
							ImGui::Checkbox("Spread Crosshair", &g_Options.Visuals.SpreadCrosshair);
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##miscchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Misc");
							ImGui::SliderFloat("##fovmodifier", &g_Options.Visuals.viewmodelChanger, -200.f, 200.f, "Fov Modifier: %.f");
							ImGui::Checkbox("Grenade Trajectory", &g_Options.Visuals.GrenadePrediction);
							ImGui::Combo("Chams", &g_Options.Visuals.SceneChams, chams_sceneend, ARRAYSIZE(chams_sceneend));
							ImGui::Checkbox("Fake-Angle Chams", &g_Options.Visuals.GhostChams);
							ImGui::Checkbox("Glow Outline", &g_Options.Visuals.Glow);
							ImGui::Checkbox("Draw AA-Side", &g_Options.Visuals.ArrowAA);
							ImGui::Checkbox("Render Skinchanger", &g_Options.Menu.Skinchanger1);
							ImGui::Hotkey("TP Key", &g_Options.Visuals.ThirdPersonKey);
						} ImGui::EndChild();
					}
					else if (Global::CurrentTab == 5) {
						ImGui::BeginChild("##boxesp", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Box");
							ImGui::ColorEdit3("TT Visible", g_Options.Visuals.VisibleT);
							ImGui::ColorEdit3("TT Invisible", g_Options.Visuals.InvisibleT);
							ImGui::ColorEdit3("CT Visible", g_Options.Visuals.VisibleCT);
							ImGui::ColorEdit3("CT Invisible", g_Options.Visuals.InvisibleCT);
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##chamsesp", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Chams");
							ImGui::ColorEdit3("Enemy", g_Options.Visuals.ChamsT);
							ImGui::ColorEdit3("Team", g_Options.Visuals.ChamsCT);
							ImGui::ColorEdit3("XQZ Enemy", g_Options.Visuals.XQZT);
							ImGui::ColorEdit3("XQZ Team", g_Options.Visuals.XQZCT);
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##glowesp", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Glow");
							ImGui::ColorEdit3("Enemy", g_Options.Visuals.GlowTT);
							ImGui::ColorEdit3("Team", g_Options.Visuals.GlowCT);
						} ImGui::EndChild();
					}
					else if (Global::CurrentTab == 4) {
						ImGui::BeginChild("##generalmisc", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Main");
							ImGui::Combo("Bunnyhop", &g_Options.Misc.Bhop, bhopstyles, ARRAYSIZE(bhopstyles));
							ImGui::Checkbox("Auto-Strafer", &g_Options.Misc.AutoStrafe);
							ImGui::Checkbox("Left-Hand on Knife", &g_Options.Misc.LeftHand1);
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##othermisc", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Other");
							ImGui::Checkbox("Infobox", &g_Options.Misc.InfoBox);
							ImGui::Checkbox("Spectator List", &g_Options.Misc.SpecList);
							ImGui::Checkbox("Clantag", &g_Options.Misc.syncclantag);
							ImGui::Checkbox("Namechanger", &g_Options.Misc.silentstealer);
							ImGui::SameLine();
							if (ImGui::Button("Silent"))
							{
								static ConVar* name = g_CVar->FindVar("name");
								if (name)
								{
									*(int*)((DWORD)&name->fnChangeCallback + 0xC) = NULL;
									name->SetValue("\n");
								}
							}
						} ImGui::EndChild();
						ImGui::SameLine();
						ImGui::BeginChild("##configchild", ImVec2(250, 350), true);
						{
							ImGui::Text("			  Config");
							ImGui::PushItemWidth(145);
							ImGui::InputText(("File"), &g_Options.Menu.ConfigFile, 50);
							if (ImGui::Button("Save/Create")) Config->Save();
							ImGui::SameLine();
							if (ImGui::Button("Load")) {
								Config->Load();
								KnifeApplyCallbk();
							}
							ImGui::SameLine();
							if (ImGui::Button("Unhook"))
							{
								g_Engine->ClientCmd_Unrestricted("cl_mouseenable 1");
								unload = true;
							}
						} ImGui::EndChild();
					}
				} ImGui::EndChild();

			}ImGui::End();




		}
	}

	if (g_Options.Menu.ColorWindow) {
		RenderColorWindow();
		g_Options.Menu.ColorWindow = false;
	}

}
