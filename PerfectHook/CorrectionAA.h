#include "CreateMove.h"

bool eqqual(float x, float y, float diff)
{
	return std::abs(x - y) <= std::abs(diff);
}

static int missedShots[65];
bool isPartOf(char *a, char *b) {
	if (std::strstr(b, a) != NULL) {    //Strstr says does b contain a
		return true;
	}
	return false;
}

class CResolver {
public:
	void AntiAimResolver(C_BaseEntity* pEntity);
};

struct ResolverData
{
	float simtime, flcycle[13], flprevcycle[13], flweight[13], flweightdatarate[13], fakewalkdetection[2], fakeanglesimtimedetection[2], fakewalkdetectionsimtime[2];
	float yaw, addyaw, lbycurtime;
	float shotsimtime, oldlby, lastmovinglby, balanceadjustsimtime, balanceadjustflcycle;
	int fakeanglesimtickdetectionaverage[4], amountgreaterthan2, amountequal1or2, amountequal0or1, amountequal1, amountequal0, resetmovetick, resetmovetick2;
	int tick, balanceadjusttick, missedshots, activity[13];
	bool bfakeangle, bfakewalk, playerhurtcalled, weaponfirecalled;
	Vector shotaimangles, hitboxPos, balanceadjustaimangles;
	uint32_t norder[13];
	char* resolvermode = "None", *fakewalk = "Not Moving";
};

extern ResolverData pResolverData[65];
extern CResolver* Resolver;

void CResolver::AntiAimResolver(C_BaseEntity* pEntity)
{
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	int sequence_number;
	CInput::CUserCmd *cmd = g_Input->GetUserCmd(0, sequence_number);
	int i = pEntity->GetIndex();
	static int shotsfired[65];
	static float MoveLBY[65];
	static float FirstDelta[65];
	static bool fakeWalking;
	if (pWeapon)
	{
		if (pLocal->m_iShotsFired() > 1 && cmd->buttons & IN_ATTACK)
			shotsfired[65]++;
	}
	if (!pLocal->IsAlive())
		missedShots[i] = 0;

	if (pEntity->GetFlags() & FL_ONGROUND)
	{
		FirstDelta[i] = abs(pEntity->GetEyeAngles()->y - pEntity->GetLowerBodyYaw());

		if (fakeWalking)
		{
			pResolverData[i].resolvermode = "Inverse";
			pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y + 180.f;
		}
		if (pEntity->GetVelocity().Length2D() > 15)
		{
			shotsfired[i] = 0;
			FirstDelta[i] = 0;
			pResolverData[i].resolvermode = "Moving";
			MoveLBY[i] = pEntity->GetLowerBodyYaw();
			pEntity->GetEyeAngles()->y = pEntity->GetLowerBodyYaw();
		}
		else
		{
			if (MoveLBY[i] = 0)
				MoveLBY[i] = pEntity->GetLowerBodyYaw();
			if (pEntity->GetVelocity().Length2D() > 100 && pEntity->GetEyeAngles()->y != pEntity->GetLowerBodyYaw())
			{
				pResolverData[i].resolvermode = "Break LBY";
				if (FirstDelta[i] < 110 && FirstDelta[i] > -110)
					pEntity->GetEyeAngles()->y = FirstDelta[i] - 120;
				else if (FirstDelta[i] < 35 && FirstDelta[i] > -35)
					pEntity->GetEyeAngles()->y = FirstDelta[i] + 180;
				else
					pEntity->GetEyeAngles()->y = FirstDelta[i];
			}
			else
			{
				if (eqqual(FirstDelta[i], 0, 40.f))
				{
					pResolverData[i].resolvermode = "Flip";
					pEntity->GetEyeAngles()->y = (cmd->command_number % 2 ? MoveLBY[i] : FirstDelta[i]);
				}
				else if (missedShots[i] >= 2 && missedShots[i] <= 4)
				{
					pResolverData[i].resolvermode = "Fix LBY";
					pEntity->GetEyeAngles()->y = FirstDelta[i] + 180;
				}
				else
				{
					pResolverData[i].resolvermode = "Restored LBY";
					pEntity->GetEyeAngles()->y = MoveLBY[i];
				}
			}
		}
	}
	else
	{
		pResolverData[i].resolvermode = "In Air";
		if (shotsfired[i] > 7 || !pLocal->IsAlive() || !pWeapon)
			shotsfired[i] = 0;

		switch (shotsfired[i])
		{
		case 1: pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y + 180.f; break;
		case 2: pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y - 120.f; break;
		case 3: pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y + 90.f; break;
		case 4: pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y - 45.f; break;
		case 5: pEntity->GetEyeAngles()->y = pEntity->GetEyeAngles()->y + 15.f; break;
		case 6: pEntity->GetEyeAngles()->y = pEntity->GetLowerBodyYaw() - 120.f;

		}

		if (pEntity->GetEyeAngles()->x < -179.f) pEntity->GetEyeAngles()->x += 360.f;
		else if (pEntity->GetEyeAngles()->x > 90.0 || pEntity->GetEyeAngles()->x < -90.0) pEntity->GetEyeAngles()->x = 89.f;
		else if (pEntity->GetEyeAngles()->x > 89.0 && pEntity->GetEyeAngles()->x < 91.0) pEntity->GetEyeAngles()->x -= 90.f;
		else if (pEntity->GetEyeAngles()->x > 179.0 && pEntity->GetEyeAngles()->x < 181.0) pEntity->GetEyeAngles()->x -= 180;
		else if (pEntity->GetEyeAngles()->x > -179.0 && pEntity->GetEyeAngles()->x < -181.0) pEntity->GetEyeAngles()->x += 180;
		else if (fabs(pEntity->GetEyeAngles()->x) == 0) pEntity->GetEyeAngles()->x = std::copysign(89.0f, pEntity->GetEyeAngles()->x);
	}
}

ResolverData pResolverData[65];
CResolver* Resolver;