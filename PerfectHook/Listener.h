#pragma once
#include "singleton.hpp"
#include "MiscClasses.h"
#include "Interfaces.h"

class item_purchase
    : public singleton<item_purchase>
{
    class item_purchase_listener
        : public IGameEventListener2
    {
    public:
        void start()
        {
            g_EventManager->AddListener(this, "item_purchase", false);
        }
        void stop()
        {
            g_EventManager->RemoveListener(this);
        }
        void FireGameEvent(IGameEvent *event) override
        {
			singleton()->on_fire_event(event);
        }
        int GetEventDebugID(void) override
        {
            return 42 /*0x2A*/;
        }
    };
public:
    static item_purchase* singleton()
    {
        static item_purchase* instance = new item_purchase;
        return instance;
    }

    void initialize()
    {
        listener.start();
    }

    void remove()
    {
        listener.stop();
    }

	typedef void(__cdecl* MsgFn)(const char* msg, va_list);
	void Msg(const char* msg, ...)
	{

		if (msg == nullptr)
			return; //If no string was passed, or it was null then don't do anything
		static MsgFn fn = (MsgFn)GetProcAddress(GetModuleHandle("tier0.dll"), "Msg"); //This gets the address of export "Msg" in the dll "tier0.dll". The static keyword means it's only called once and then isn't called again (but the variable is still there)
		char buffer[989];
		va_list list; //Normal varargs stuff http://stackoverflow.com/questions/10482960/varargs-to-printf-all-arguments
		va_start(list, msg);

		vsprintf(buffer, msg, list);
		va_end(list);

		fn(buffer, list); //Calls the function, we got the address above.
	}

	player_info_t GetInfo(int Index) {
		player_info_t Info;
		g_Engine->GetPlayerInfo(Index, &Info);
		return Info;
	}

    void on_fire_event(IGameEvent* event)
    {
		g_Engine->ClientCmd_Unrestricted("developer 1");
		g_Engine->ClientCmd_Unrestricted("con_filter_enable 2");

		static auto damagefilter = g_CVar->FindVar("con_filter_text");
		damagefilter->SetValue("[nost�lgia]");


        C_BaseEntity* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
        if (!strcmp(event->GetName(), "item_purchase"))
        {
            auto buyer = event->GetInt("userid");
            std::string gun = event->GetString("weapon");


            if (strstr(gun.c_str(), "molotov")
                || strstr(gun.c_str(), "nade")
                || strstr(gun.c_str(), "kevlar")
                || strstr(gun.c_str(), "decoy")
                || strstr(gun.c_str(), "suit")
                || strstr(gun.c_str(), "flash")
                || strstr(gun.c_str(), "vest")
                || strstr(gun.c_str(), "cutter")
                || strstr(gun.c_str(), "defuse")
                )  return;
				

            auto player_index = g_Engine->GetPlayerForUserID(buyer);
            C_BaseEntity* player = (C_BaseEntity*)g_EntityList->GetClientEntity(player_index);
            player_info_t pinfo;
            
            if (player && local &&  player->GetTeamNum() != local->GetTeamNum() && g_Engine->GetPlayerInfo(player_index, &pinfo))
            {

                if (g_ChatElement)
                {
                    gun.erase(gun.find("weapon_"), 7);
                   // g_ChatElement->ChatPrintf(0, 0, " ""\x04""%s bought %s\n", pinfo.name, gun.c_str());
					Msg("[nostalgia] ""\x04""%s bought %s\n", pinfo.name, gun.c_str());
                }
            }
        }
    }
private:
    item_purchase_listener  listener;
};
