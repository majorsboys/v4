#pragma once

extern void RenderInterface();
extern void RenderInfoBox();
extern void RenderSpecList();
extern void RenderC4Info();
//extern void RenderEventLog(IGameEvent* event);

struct Variables
{
	Variables()
	{

	}

	struct Ragebot_s
	{
		bool MainSwitch;
		bool 	Enabled;
		bool 	AutoFire;
		float 	FOV;
		float	VelLimit;
		bool 	Silent;
		bool AutoPistol;
		int KeyPress;
		bool AimStep;
		

			bool	EnabledAntiAim;
			int		Pitch;
			int     EdgePitch;
			int     EdgeReal;
			int     EdgeFak1e;
			int		YawTrue;
			int		YawFake;
			int		EdgeYaw;
			int		EdgeFake;
			int		RealMove;
			int		FakeMove;
			bool	AtTarget;
			int	EdgeMode1;
			int SwitchKey;
			float JitterMod;
			bool JitterEnabl;
			bool KnifeAA;
			bool FreezePeriod;


			bool FriendlyFire;
			int		Hitbox;
			int		Hitscan;
			float Pointscale;
			bool Multipoint;
			float Multipoints;


			bool AntiRecoil;
			bool AutoWall;
			int AimbotType;
			bool AutoStop;
			bool AutoCrouch;
			bool AutoScope;
			float MinimumDamage;
			bool Hitchance;
			float HitchanceAmount;
			float HitchanceMax;
			bool Resolver;
			int LbyFix;
			int pResolvery;
			bool AdvancedCorrection;
			bool Resolver2;
			float YawAngle;
			int PitchAngle;
			bool CheckEdge;
			bool FakeWalk;
			bool FakeLagEnable;
			float FakeLagPower;
			int FakeLagMode;
			int FakeWalkKey;
			bool AntiResolvery;
			float LBYDelta;
			float BruteforceX;
			bool FakeLagFix;
			bool playerlist;
            int BAIMkey;
			float BaimIFHPX;
	} Ragebot;

	struct
	{
		bool MainSwitch;
        bool backtrack;
		bool LegitAA;
		float BacktrackTicks;
		struct
		{
			bool 	Enabled;
			bool AutoPistol;
			bool	Resolver;
            
		} Aimbot;


			int MainKey = 1;
            float MainSmooth;
			float Mainfov;
            float main_random_smooth;
            float main_recoil_min;
            float main_recoil_max;
            float main_randomized_angle;



			int PistolKey = 6;
			float Pistolfov;
			float PistolSmooth;
            float pistol_random_smooth;
            float pistol_recoil_min;
            float pistol_recoil_max;
            float pistol_randomized_angle;



			int SniperKey = 6;
			float Sniperfov;
			float SniperSmooth;
            float sniper_random_smooth;
            float sniper_recoil_min;
            float sniper_recoil_max;
            float sniper_randomized_angle;


		struct
		{
			bool	Enabled;
			float Delay = 1.f;
			int Key;
			float hitchance;
			struct
			{
				bool Head;
				bool Arms;
				bool Chest;
				bool Stomach;
				bool Legs;
			} Filter;

		} Triggerbot;

	} Legitbot;

	struct
	{
		bool 	Enabled;
		bool	Fpsboost11;

		bool Box;
		bool Name;
		bool HP;
		bool BoxColor;
		bool Weapon;
		bool Info;
		bool Chams;
		bool Skeleton;
		bool AimSpot;
		bool DLight;
		bool SpreadCrosshair;
		bool GrenadeESP;
		bool C4;

		bool Glow;
		bool GhostChams;
		bool ArrowAA;
		int SceneChams;
		bool XQZChams;

		bool ShakePunch;
		bool InverseView;
		bool SniperCrosshair;
		bool NoVisualRecoil;
		int Hands;
		int Weapons;
		float FOVChanger;
		float viewmodelChanger;
		bool NoFlash;
		bool NoSmoke;
		bool SpectList;
		bool ThirdPerson;
		int ThirdPersonKey;
		int ThirdpersonMode;
		bool SkinchangerWindow;
		bool Time;
		float VisibleT[4];
		float InvisibleT[4];
		float VisibleCT[4];
		float InvisibleCT[4];
		float ChamsT[4];
		float ChamsCT[4];
		float XQZT[4];
		float XQZCT[4];
		float GlowTT[4];
		float GlowCT[4];
		float g_fBColor[4];
		float g_fTColor[4];

		bool money;

		bool C4World;
		bool WeaponsWorld;
		bool noscopeborder;
		bool GrenadePrediction;

		struct
		{
			bool Players;
			bool Enemy;
			bool VisibleOnly;
		} Filter;

		struct
		{
			bool Players;
			bool Enemy;
			bool Weapons;
		} GlowFilter;

	} Visuals;

	struct
	{
        bool silentstealer;
        int ragequit;
		int 	Bhop;
		int NasaSpeed;
		bool 	AutoStrafe;
		bool	LeftHand1;
		bool	LBYBreak;
		bool InfoBox;
		float MinVel;
		int		AirStuckKey;
		int lagexploit;
		int lagexploitmultiplier = 3;
		float lagexploitvalue;
		float FakeLag;
		bool AdaptiveFakeLag;
        bool nightMode;
		int Themes;
		int NameSpammer;
		bool NameChangerFix;
		bool NoName;
		int		ChatSpamMode;
		bool ClantagChanger;
		int ClanTagSpeed;
        bool syncclantag;
		bool animated_clan;
		bool AntiAimLines;
		bool FakeIndic;
		bool RealIndic;
		bool LBYIndic;
		bool PastaCrasher11;
		bool SpecList;
		bool ESPPrev;
		bool FPSBooster;
		int SkyBoxChanger;
        bool namespam;
		bool TrashtalkerMeme;
		bool ServerRankShow;
        int spammer;
		int AutoDefuse;
		bool Spam;
		bool isRecording;
		bool isReplaying;
		bool RecordPath;
		bool AutoAccept;
		bool SpoofConfirmation = false;
		bool animatedclantag = false;
		int customtab;

        bool niggatest;


	} Misc;
	struct
	{
		bool Enabled;
		int Knife;
        int gloves;
		int KnifeSkin;
		int AK47Skin;
		int M4A1SSkin;
		int M4A4Skin;
		int AUGSkin;
		int FAMASSkin;
		int AWPSkin;
		int SSG08Skin;
		int SCAR20Skin;
		int P90Skin;
		int UMP45Skin;
		int GlockSkin;
		int USPSkin;
		int DeagleSkin;
		int tec9Skin;
		int P2000Skin;
		int P250Skin;
	} Skinchanger;

	struct
	{
		bool	Opened = false;
		bool	ColorWindow = false;
		bool	Infobox = true;
		int 	Key;
		bool	Ragebot = false;
		char	username;
		char	password;
		bool	Legitbot = false;
		bool	Visual = false;
		bool	Misc = false;
		char	ConfigFile;
		int		Theme = 0;
		bool	Colors = false;
		bool Ragebot1 = false;
		bool Legit1 = false;
		bool Visuals1 = false;
		bool CONFIGS1 = false;
		bool Misc1 = false;
		bool Skinchanger1 = false;
		bool ConsoleWindow = false;
		bool HvH1 = false;
	} Menu;
};

extern Variables g_Options;