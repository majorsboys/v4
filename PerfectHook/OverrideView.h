#pragma once
#include "HookIncludes.h"

typedef void(__thiscall *override_view_t)(void* _this, CViewSetup* setup);
typedef float(__thiscall *get_fov_t)(void*);



float __fastcall hkGetViewModelFOV(void* ecx, void* edx)
{
    static auto ofunc = hooks::clientmode.get_original<get_fov_t>(35);
    float viewmodelFOV = ofunc(ecx);
    return g_Options.Visuals.viewmodelChanger;
}

void __fastcall hkOverrideView(void* _this, void* _edx, CViewSetup* setup)
{
    static auto ofunc = hooks::clientmode.get_original<override_view_t>(18);
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	if (pLocal && g_Engine->IsInGame())
	{
		if (g_Options.Visuals.ShakePunch)
		{
			if (pLocal->localPlayerExclusive()->GetAimPunchAngle().y > 20 || pLocal->localPlayerExclusive()->GetAimPunchAngle().y > 40) {
				setup->angles += ((pLocal->localPlayerExclusive()->GetAimPunchAngle().x * 20 / 10 * 5));
			}else
				setup->angles += ((pLocal->localPlayerExclusive()->GetAimPunchAngle().y * 90 / 10 * 100));
		}

		if (g_Options.Visuals.SniperCrosshair) {
			CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
			static auto debugspread = g_CVar->FindVar("weapon_debug_spread_show");

			if (jdhfa78637asghdjagsudiayr87aysjdhauia3::IsSniper(pWeapon))
				debugspread->SetValue(3);
			else
				debugspread->SetValue(0);
		}

		if (g_Options.Visuals.InverseView)
		{
			setup->angles += 180;
			g_Engine->ClientCmd_Unrestricted("crosshair 0");
		}
		else
			g_Engine->ClientCmd_Unrestricted("crosshair 1");

		//if (g_Options.Visuals.NoVisualRecoil)
		//{
			//setup->angles -= pLocal->localPlayerExclusive()->GetAimPunchAngle() * 2;
		//}
		if (!pLocal->IsScoped())
			setup->fov += g_Options.Visuals.FOVChanger;

		static bool enabledtp = false;
		static bool check = false;
		if (GetAsyncKeyState(g_Options.Visuals.ThirdPersonKey))
		{
			if (!check)
				enabledtp = !enabledtp;
			check = true;
		}
		else
			check = false;


        if (enabledtp && pLocal->IsAlive())
        {
            if (!g_Input->m_fCameraInThirdPerson)
            {
                g_Input->m_fCameraInThirdPerson = true;
            }
        }
        else
        {
            g_Input->m_fCameraInThirdPerson = false;
        }
	}
    tyletoijapamieamuiashu8fyeuiwas::instance().View(setup);
    ofunc(_this, setup);
}
